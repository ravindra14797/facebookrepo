<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WrongEmail extends Model
{
    //
    protected $fillable = ["email","problem_type"];
}

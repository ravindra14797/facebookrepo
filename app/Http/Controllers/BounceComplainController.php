<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WrongEmail;
use Log;

class BounceComplainController extends Controller
{
  public function handleBounceOrComplaint(Request $request)
 {
    Log::info($request->json()->all());
    $data = $request->json()->all();
    if($request->json('Type') == 'SubscriptionConfirmation')
      Log::info("SubscriptionConfirmation came at: ".$data['Timestamp']);
    if($request->json('Type') == 'Notification'){
      $message = $request->json('Message');
      $message = json_decode($message);
      switch($message->notificationType){
        case 'Bounce':
          $bounce = $message->bounce;
          foreach ($bounce->bouncedRecipients as $bouncedRecipient){
            $emailAddress = $bouncedRecipient->emailAddress;
            $emailRecord = WrongEmail::firstOrCreate(['email' => $emailAddress, 'problem_type' => 'Bounce']);
            if($emailRecord){
              $emailRecord->increment('repeated_attempts',1);
            }
          }
          break;
        case 'Complaint':
          $complaint = $message['complaint'];
          foreach($complaint['complainedRecipients'] as $complainedRecipient){
            $emailAddress = $complainedRecipient['emailAddress'];
            $emailRecord = WrongEmail::firstOrCreate(['email' => $emailAddress, 'problem_type' => 'Complaint']);
            if($emailRecord){
              $emailRecord->increment('repeated_attempts',1);
            }
          }
          break;
        default:
          // Do Nothing
          break;
      }
    }
    return response()->json(['status' => 200, "message" => 'success']);
  }
}

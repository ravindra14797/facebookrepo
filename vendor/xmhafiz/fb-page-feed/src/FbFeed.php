<?php
/**
 * Description of FbFeed
 *
 * @author hafiz
 */
namespace Xmhafiz\FbFeed;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Carbon\Carbon;

class FbFeed {

    private $app_id = null;
    private $secret_key = null;
    private $page_name = null;
    private $keyword = null;
    private $limit = 100; // all of it
    private $fields = 'id,message,created_time,from,permalink_url,full_picture';
    public $fb;
    /**
     * FbFeed constructor.
     */
    public function __construct()
    {
        $this->secret_key =  getenv('FB_SECRET_KEY');
        $this->app_id = getenv('FB_APP_ID');
        $this->page_name = getenv('FB_PAGENAME');
       
    }


    /**
     * @param $app_id
     * @return $this
     */
    function setAppId($app_id) {
        $this->app_id = $app_id;
        return $this;
    }

    /**
     * @param $app_id
     * @param $secret_key
     * @return $this
     */
    function setCredential($app_id, $secret_key) {
        $this->app_id = $app_id;
        $this->secret_key = $secret_key;
        return $this;
    }

    /**
     * @param $secret_key
     * @return $this
     */
    function setSecretKey ($secret_key) {
        $this->secret_key = $secret_key;
        return $this;
    }

    /**
     * @param $fields
     * @return $this
     */
    function fields($fields) {
        $this->fields = $fields;
        return $this;
    }

    /**
     * @param $page_name
     * @return $this
     */
    function setPage($page_name) {
        $this->page_name = $page_name;
        return $this;
    }

    /**
     * @param $keyword
     * @return $this
     */
    function findKeyword($keyword) {
        $this->keyword = $keyword;
        return $this;
    }

    /**
     * @param int $total
     * @return $this
     */
    function feedLimit($total = 20) {
        $this->limit = $total;
        return $this;
    }

    /**
     * @return array
     */
    function fetch() {
        $client = new Client();

        if (!$this->page_name) {
            return $this->returnFailed('Page Name is needed');
        }

        if (!$this->app_id) {
            return $this->returnFailed('Facebook App ID is needed. Please refer to https://developers.facebook.com');
        }

        if (!$this->secret_key) {
            return $this->returnFailed('Facebook Secret Key is needed. Please refer to https://developers.facebook.com');
        }

        $fb = app(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
        $fb->setDefaultAccessToken ("EAAfD4ZBufGMwBAIowknXtKVqKdlqrf1IafkEbMLnjq9dlxknYTKk0RaZClojDwaiilsk1lVHLhuHD5FoW0PEZBfkXnhp2umSRZA5fCSQfZBbjPUYuoqagzjZACuVpiqZBrMn1jzAQRRSshbOZC6f1ymUQ0tVZCc4pJWiAuZBxSqQizluY4mpNs56ASbNOZCCt1S8sQZD");

        $latestFeeds = [];
       
        try {
            
            $response = $fb->get('/me/feed');
            $latestFeeds = $response->getGraphEdge ();

        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }


         /* @var GraphNode $feed */
        foreach ($latestFeeds->getIterator () as $feed) {
            $content = $feed->getField('message');
            /* @var \DateTime $date */
            $date = $feed->getField('created_time');
            $from = ($feed->getField('from')) ? $feed->getField('from')->asArray() : "";
            $attachment = [];
            if ($feed->getField ('attachments')){
                foreach ($feed->getField ('attachments')->getIterator() as $attatchmentIterator){
                    $attachment = $attatchmentIterator->asArray();
                }
            }

            $feeds[] = [
                'content' => $content,
                'attachment' => $attachment,
                'date' => new Carbon($date->format('r')),
                'from' => $from
            ];
        }

        return response()->json($feeds);
    }

    private function returnSuccess($feeds = null, $code = 200) {
        return [
            'error' => false,
            'status_code' => $code,
            'data' => $feeds
        ];
    }

    private function returnFailed($message = null, $code = 500) {
        return [
            'error' => true,
            'status_code' => $code,
            'message' => ($message) ? $message : 'Unexpecetd error occurred'
        ];
    }

    private function contains($keywords, $string) {
        foreach ($keywords as $needle) {
            if (stripos($string, $needle) === false) {
                return false;
            }
        }

        return true;
    }
}
